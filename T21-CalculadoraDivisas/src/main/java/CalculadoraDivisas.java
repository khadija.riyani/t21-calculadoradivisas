import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.AbstractListModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTextField;

public class CalculadoraDivisas extends JFrame {

	private JPanel fondo;
	private JTextField escrito1;
	private JTextField escrito2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CalculadoraDivisas frame = new CalculadoraDivisas();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CalculadoraDivisas() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 581, 469);
		fondo = new JPanel();
		fondo.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(fondo);
		fondo.setLayout(null);
		
		JButton btnCE = new JButton("CE");
		btnCE.setFont(new Font("Verdana", Font.BOLD, 14));
		btnCE.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				escrito1.setText("0");
				escrito1.grabFocus();
				
				escrito2.setText("0");
				escrito2.grabFocus();
			}
		});
		btnCE.setBounds(367, 56, 89, 54);
		fondo.add(btnCE);
		
		JButton btnFlecha = new JButton("\u2190");
		btnFlecha.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnFlecha.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnFlecha.setBounds(466, 56, 89, 54);
		fondo.add(btnFlecha);
		
		JButton btn9 = new JButton("9");
		btn9.setFont(new Font("Tahoma", Font.BOLD, 14));
		btn9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				escrito1.setText(escrito1.getText() + "9");
				escrito2.setText(escrito2.getText() + "9");

			}
		});
		btn9.setBounds(466, 121, 89, 54);
		fondo.add(btn9);
		
		JButton btn8 = new JButton("8");
		btn8.setFont(new Font("Tahoma", Font.BOLD, 14));
		btn8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				escrito1.setText(escrito1.getText() + "8");
				escrito2.setText(escrito2.getText() + "8");

			}
		});
		btn8.setBounds(367, 121, 89, 54);
		fondo.add(btn8);
		
		JButton btn7 = new JButton("7");
		btn7.setFont(new Font("Tahoma", Font.BOLD, 14));
		btn7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				escrito1.setText(escrito1.getText() + "7");
				escrito2.setText(escrito2.getText() + "7");

				
				
			}
		});
		btn7.setBounds(268, 121, 89, 54);
		fondo.add(btn7);
		
		JButton btn6 = new JButton("6");
		btn6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				escrito1.setText(escrito1.getText() + "6");
				escrito2.setText(escrito2.getText() + "6");

			}
		});
		btn6.setFont(new Font("Tahoma", Font.BOLD, 14));
		btn6.setBounds(466, 186, 89, 54);
		fondo.add(btn6);
		
		JButton btn5 = new JButton("5");
		btn5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				escrito1.setText(escrito1.getText() + "5");
				escrito2.setText(escrito2.getText() + "5");
				

			}
		});
		btn5.setFont(new Font("Tahoma", Font.BOLD, 14));
		btn5.setBounds(367, 186, 89, 54);
		fondo.add(btn5);
		
		JButton btn4 = new JButton("4");
		btn4.setFont(new Font("Tahoma", Font.BOLD, 14));
		btn4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				int N = '4';
				
				recorreNum(N);
				
				escrito1.setText(escrito1.getText() + "4");
				escrito2.setText(escrito2.getText() + "3");
				
				
				
			}
		});
		btn4.setBounds(268, 186, 89, 54);
		fondo.add(btn4);
		
		JButton btn3 = new JButton("3");
		btn3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				escrito1.setText(escrito1.getText() + "3");
				escrito2.setText(escrito2.getText() + "3");
			}
		});
		btn3.setFont(new Font("Tahoma", Font.BOLD, 14));
		btn3.setBounds(466, 251, 89, 54);
		fondo.add(btn3);
		
		JButton btn2 = new JButton("2");
		btn2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				escrito1.setText(escrito1.getText() + "2");
				escrito2.setText(escrito2.getText() + "2");
			}
		});
		btn2.setFont(new Font("Tahoma", Font.BOLD, 14));
		btn2.setBounds(367, 251, 89, 54);
		fondo.add(btn2);
		
		JButton btn1 = new JButton("1");
		btn1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				escrito1.setText(escrito1.getText() + "1");
				escrito2.setText(escrito2.getText() + "1");
			}
		});
		btn1.setFont(new Font("Tahoma", Font.BOLD, 14));
		btn1.setBounds(268, 251, 89, 54);
		fondo.add(btn1);
		
		JButton btnComa = new JButton(",");
		btnComa.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnComa.setBounds(466, 316, 89, 54);
		fondo.add(btnComa);
		
		JButton btn0 = new JButton("0");
		btn0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				escrito1.setText(escrito1.getText() + "0");
				escrito2.setText(escrito2.getText() + "0");
			}
		});
		btn0.setFont(new Font("Tahoma", Font.BOLD, 14));
		btn0.setBounds(367, 316, 89, 54);
		fondo.add(btn0);
		
		JComboBox combobox1 = new JComboBox();
		combobox1.setModel(new DefaultComboBoxModel(new String[] {"Dolar", "Euro"}));
		combobox1.setBounds(39, 186, 78, 22);
		fondo.add(combobox1);
		combobox1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		JComboBox combobox2 = new JComboBox();
		combobox2.setModel(new DefaultComboBoxModel(new String[] {"Dolar", "Euro"}));
		combobox2.setBounds(39, 354, 78, 22);
		fondo.add(combobox2);
		
		escrito1 = new JTextField();
		escrito1.setBounds(31, 75, 214, 100);
		fondo.add(escrito1);
		escrito1.setColumns(10);
		
		escrito2 = new JTextField();
		escrito2.setColumns(10);
		escrito2.setBounds(31, 230, 214, 100);
		fondo.add(escrito2);
		
		JButton calcular = new JButton("Calcular");
		calcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				double Moneda = Double.parseDouble(escrito1.getText());
				double Resultado;
				
				Resultado = 166.386 / Moneda ;
						
				escrito1.setText(Resultado);
				escrito2.setText(Resultado);
				 //JOptionPane.showInputDialog( Moneda + " En Euros : " + Resultado);
				 
				 //escrito1.append( Moneda + " En Euros : " + Resultado);
				
				
			}
		});
		calcular.setBounds(31, 396, 89, 23);
		fondo.add(calcular);
	}
	public void recorreNum(int N) {
		recorreNum(N);
		
		}
}
